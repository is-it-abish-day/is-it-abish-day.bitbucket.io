var modelOpen = false;
const now = new Date();
const currentMonth = now.getMonth();
const currentDate = now.getDate();
const isBirthday = currentMonth === 0 && currentDate === 9 ? true : false;
if (!isBirthday) {
    year = currentMonth != 0 ? now.getFullYear() + 1 : now.getFullYear();
    theDday = new Date(year,00,9);
    time = moment(theDday, "YYYYMMDD").fromNow();
    text = "His birthday is " + time;
    elem = document.getElementById("caption");
    cur = moment(new Date());
    duration = moment.duration(cur.diff(theDday));
    seconds = Math.floor(Math.abs(duration.asSeconds()));
    elem.append(text);
    startCounting(seconds);
} else {
    elem = document.getElementById("answer");
    elem.innerHTML = "YES!"
    elem2 = document.getElementById("waiting");
    elem2.classList.add("is-hidden");
}

function startCounting(seconds){
    seconds = Math.floor(Math.abs(seconds))
    elem = document.getElementById("seconds");
    setInterval(()=>{
        text = "കുറച്ചുകൂടി തള്ളുകയാണെങ്കിൽ നമുക്ക് ഇങ്ങനേം പറയാം: <b>"+seconds+"</b> സുന്ദര നിമിഷങ്ങൾ😍."
        if(!modelOpen){
            elem.innerHTML = text;
        }
        seconds--;
        if(seconds<0){
            location.reload;
        }
    },1000)
}

document.getElementById("close").addEventListener('click',(event)=>{
    location.reload();
})

document.getElementById("pics").addEventListener('click', (event) => {
    modelOpen = true;
    elem = document.getElementById("modal");
    elem.classList.add("is-active");
    pic = Math.floor((Math.random() * 33) + 1);
    url = 'assets/images/'+pic+'.jpg';
    picElem = document.getElementById("random");
    picElem.setAttribute("src",url)
})
